
# OmfgAuth

...because life's too short for reading OAuth 2.0 specs

# What is it

A hopefully easy-to-understand specification for a self-hosted, webbased Single
Sign On.

# Why

because i am to stupid to get OAuth 2.0 from the specs, to scared to "just use a
library" i don't understand to a level that satisfies me, and in general
frustrated by people who think that Oauth 2.0 will be used for other things than
driving users in the arms of some few monopolies which may have the resources
to provide the ecosystem needed to provide said standard, when getting a senior
to understand it will cost more time than a junior implementing another halfway
sound password based login.
And yes, i am fully aware of [the relevant xkcd](https://xkcd.com/927/).

# How it works

    AuthServer                   Browser                        Auth Consumer
    |                               |                         e.G. "your website"
    |                               |                                 |
    |                               | "i can haz private page?"       |
    |                               | GET /private                    |
    |                               |-------------------------------->|    (1)
    |                               |                                 |
    |                               | "ask here for auth first"       |
    |                               | 302 authserver.com/?for=...     |
    |                               |<--------------------------------|
    |                               |                                 |
    |                               |                                 |
    | "i can haz auth?"             |                                 |
    | GET authserver.com/?for=...   |                                 |
    |<------------------------------|                                 |    (2)
    |                               |                                 |
    |                               |                                 |

        whatever hoops the authserver makes the user jump through          (3)
        to authenticate them, e.g. provide a password

    |                               |                                 |
    |                               |                                 |
    | "here you haz auth"           |                                 |
    | 302 website.com/?token=...    |                                 |
    |------------------------------>|                                 |    (4)
    |                               |                                 |
    |                               | "i can haz private page nao?"   |
    |                               | GET website.com/?token=...      |
    |                               |-------------------------------->|    (5)
    |                               |                                 |
    |                               | "here you go"                   |
    |                               | 200 and a cookie                |
    |                               |<--------------------------------|
    |                               |                                 |



Your website sends your client to the OmfgAuth Server, like this

    https://omfgauthserver.example/authenticate/?for=https://website.example/login

The OmfgAuth Server does whatever it feels to be neccessary to authenticate the
user. If that succeds, it redirects the client to the url given in the *for* parameter,
with a parameter *token*, which contains a *JsonWebToken*, like this:

    http://website.example/login?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhbGljZSIsImlzcyI6Im9tZmdhdXRoc2VydmVyLmV4YW1wbGUiLCJhdWQiOiJodHRwczovL3dlYnNpdGUuZXhhbXBsZS9sb2dpbiIsImlhdCI6MTU4NjIzOTAyMn0.B8FYyeo3oZZHtmfnbQ2UhxDG06kiDx7VRcSeevdqE_4

The token MUST contain at least these *claims*:
 
* *sub* - the subject that was authenticated by the OmfgAuthServer, e.g. "Alice"
* *iss* - the Domain of the OmfgAuth server, e.g. omfgauthserver.example
* *exp* - a near future expiry date
* *aud* - the url to which the authserver redirected the client, e.g. https://website.example/login

The website MUST check...

- whether the JWT is valid, with the method and secret configured for the value in *iss*
- whether the expiry date provided in *exp* is in the future
- whether the requested url is the one given in *aud*

If *any* of these checks fails, the website MUST ignore the request.

If *all* of these checks succeed, the website can treat the user provided in *sub*
as authenticated, and for example, create a cookie based session or the like.